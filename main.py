#!/usr/bin/env python
import builtins

from klein import Klein
from ctrader_open_api import Client, Protobuf, TcpProtocol, Auth, EndPoints
from ctrader_open_api.endpoints import EndPoints
from ctrader_open_api.messages.OpenApiCommonMessages_pb2 import *
from ctrader_open_api.messages.OpenApiMessages_pb2 import *
from ctrader_open_api.messages.OpenApiModelMessages_pb2 import *
import json
from twisted.internet import endpoints, reactor
from twisted.web.server import Site
import sys
from twisted.python import log
import datetime
from google.protobuf.json_format import MessageToJson
import calendar

host = "0.0.0.0"
port = 8085

credentialsFile = open("credentials.json")
credentials = json.load(credentialsFile)
token = ""
currentAccountId = None

app = Klein()

@app.route('/get-data/<token>/<command>', methods=['GET'])
def getData(request, token, command):
    try:        
        commandSplit = command.split("+")

        if (commandSplit[0] not in commands):
            result = f"Invalid Command: {commandSplit[0]}"
        else:
            parameters = commandSplit[1:]
            parameters.insert(0, token)
            result = commands[commandSplit[0]](*parameters)
            result.addCallback(encodeResult)
    except Exception as e:
        result = e.__str__()
    except builtins.AttributeError as e:
        result = e.__str__()

    if type(result) is str:
            result = encodeResult(result)
    return  result


def onError(failure):
    print("Message Error: \n", failure)

def connected(client):
    print("Client Connected")
    request = ProtoOAApplicationAuthReq()
    request.clientId = credentials["ClientId"]
    request.clientSecret = credentials["Secret"]
    deferred = client.send(request)
    deferred.addErrback(onError)

def disconnected(client, reason):
    print("Client Disconnected, reason: \n", reason)

def onMessageReceived(client, message):
    if message.payloadType == ProtoHeartbeatEvent().payloadType:
        return
    #print("Client Received a Message: \n", message)

authorizedAccounts = []

def setAccount(token, accountId):
    global currentAccountId
    currentAccountId = int(accountId)
    if accountId not in authorizedAccounts:
        return sendProtoOAAccountAuthReq(token, accountId)
    return '{"message":"Account changed successfully"}'.encode(encoding = 'UTF-8')

def sendProtoOAVersionReq(token, clientMsgId = None):
    request = ProtoOAVersionReq()
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAGetAccountListByAccessTokenReq(token,clientMsgId = None):
    request = ProtoOAGetAccountListByAccessTokenReq()
    request.accessToken = token
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAAccountLogoutReq(token, clientMsgId = None):
    request = ProtoOAAccountLogoutReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAAccountAuthReq(token, clientMsgId = None):
    request = ProtoOAAccountAuthReq()
    request.ctidTraderAccountId = currentAccountId
    request.accessToken = token
    deferred = client.send(request, clientMsgId=clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAAssetListReq(token, clientMsgId = None):
    request = ProtoOAAssetListReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAAssetClassListReq(token, clientMsgId = None):
    request = ProtoOAAssetClassListReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOASymbolCategoryListReq(token, clientMsgId = None):
    request = ProtoOASymbolCategoryListReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAOrderListReq(token, timeInSeconds=calendar.timegm(datetime.datetime.utcnow().utctimetuple()), clientMsgId = None):
    request = ProtoOAOrderListReq()
    request.ctidTraderAccountId = currentAccountId
    request.fromTimestamp = int(int(timeInSeconds) - 604800) * 1000
    request.toTimestamp = int(timeInSeconds) * 1000
    deferred = client.send(request)
    deferred.addErrback(onError)
    return deferred

def sendProtoOASymbolsListReq(token, includeArchivedSymbols = False, clientMsgId = None):
    request = ProtoOASymbolsListReq()
    request.ctidTraderAccountId = currentAccountId
    request.includeArchivedSymbols = includeArchivedSymbols if type(includeArchivedSymbols) is bool else bool(includeArchivedSymbols)
    deferred = client.send(request)
    deferred.addErrback(onError)
    return deferred

def sendProtoOATraderReq(token, clientMsgId = None):
    request = ProtoOATraderReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAUnsubscribeSpotsReq(token, symbolId, clientMsgId = None):
    request = ProtoOAUnsubscribeSpotsReq()
    request.ctidTraderAccountId = currentAccountId
    request.symbolId.append(int(symbolId))
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAReconcileReq(token, clientMsgId = None):
    request = ProtoOAReconcileReq()
    request.ctidTraderAccountId = currentAccountId
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAGetTrendbarsReq(token, weeks, period, symbolId, clientMsgId = None):
    request = ProtoOAGetTrendbarsReq()
    request.ctidTraderAccountId = currentAccountId
    request.period = ProtoOATrendbarPeriod.Value(period)
    request.fromTimestamp = int(calendar.timegm((datetime.datetime.utcnow() - datetime.timedelta(weeks=int(weeks))).utctimetuple())) * 1000
    request.toTimestamp = int(calendar.timegm(datetime.datetime.utcnow().utctimetuple())) * 1000
    request.symbolId = int(symbolId)
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOAGetTickDataReq(token, days, quoteType, symbolId, clientMsgId = None):
    request = ProtoOAGetTickDataReq()
    request.ctidTraderAccountId = currentAccountId
    request.type = ProtoOAQuoteType.Value(quoteType.upper())
    request.fromTimestamp = int(calendar.timegm((datetime.datetime.utcnow() - datetime.timedelta(days=int(days))).utctimetuple())) * 1000
    request.toTimestamp = int(calendar.timegm(datetime.datetime.utcnow().utctimetuple())) * 1000
    request.symbolId = int(symbolId)
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOANewOrderReq(token, symbolId, orderType, tradeSide, volume, price = None, clientMsgId = None):
    request = ProtoOANewOrderReq()
    request.ctidTraderAccountId = currentAccountId
    request.symbolId = int(symbolId)
    request.orderType = ProtoOAOrderType.Value(orderType.upper())
    request.tradeSide = ProtoOATradeSide.Value(tradeSide.upper())
    request.volume = int(volume) * 100
    if request.orderType == ProtoOAOrderType.LIMIT:
        request.limitPrice = float(price)
    elif request.orderType == ProtoOAOrderType.STOP:
        request.stopPrice = float(price)
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendNewMarketOrder(token, symbolId, tradeSide, volume, clientMsgId = None):
    return sendProtoOANewOrderReq(symbolId, "MARKET", tradeSide, volume, clientMsgId = clientMsgId)

def sendNewLimitOrder(token, symbolId, tradeSide, volume, price, clientMsgId = None):
    return sendProtoOANewOrderReq(symbolId, "LIMIT", tradeSide, volume, price, clientMsgId)

def sendNewStopOrder(token, symbolId, tradeSide, volume, price, clientMsgId = None):
    return sendProtoOANewOrderReq(symbolId, "STOP", tradeSide, volume, price, clientMsgId)

def sendProtoOAClosePositionReq(token, positionId, volume, clientMsgId = None):
    request = ProtoOAClosePositionReq()
    request.ctidTraderAccountId = currentAccountId
    request.positionId = int(positionId)
    request.volume = int(volume) * 100
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

def sendProtoOACancelOrderReq(token, orderId, clientMsgId = None):
    request = ProtoOACancelOrderReq()
    request.ctidTraderAccountId = currentAccountId
    request.orderId = int(orderId)
    deferred = client.send(request, clientMsgId = clientMsgId)
    deferred.addErrback(onError)
    return deferred

commands = {
        "setAccount": setAccount,
        "ProtoOAVersionReq": sendProtoOAVersionReq,
        "ProtoOAGetAccountListByAccessTokenReq": sendProtoOAGetAccountListByAccessTokenReq,
        "ProtoOAAssetListReq": sendProtoOAAssetListReq,
        "ProtoOAAssetClassListReq": sendProtoOAAssetClassListReq,
        "ProtoOASymbolCategoryListReq": sendProtoOASymbolCategoryListReq,
        "ProtoOASymbolsListReq": sendProtoOASymbolsListReq,
        "ProtoOATraderReq": sendProtoOATraderReq,
        "ProtoOAReconcileReq": sendProtoOAReconcileReq,
        "ProtoOAGetTrendbarsReq": sendProtoOAGetTrendbarsReq,
        "ProtoOAGetTickDataReq": sendProtoOAGetTickDataReq,
        "NewMarketOrder": sendNewMarketOrder,
        "NewLimitOrder": sendNewLimitOrder,
        "NewStopOrder": sendNewStopOrder,
        "ClosePosition": sendProtoOAClosePositionReq,
        "CancelOrder": sendProtoOACancelOrderReq,
        "ProtoOAOrderListReq": sendProtoOAOrderListReq}

def encodeResult(result):
    if type(result) is str:
        response = f'{{"result":  {result} }}'
    else:
        response =  MessageToJson(Protobuf.extract(result))
    return f'{{"get_data": {response} }}'


log.startLogging(sys.stdout)

client = Client(EndPoints.PROTOBUF_LIVE_HOST if credentials["Host"].lower() == "live" else EndPoints.PROTOBUF_DEMO_HOST, EndPoints.PROTOBUF_PORT, TcpProtocol)
client.setConnectedCallback(connected)
client.setDisconnectedCallback(disconnected)
client.setMessageReceivedCallback(onMessageReceived)
client.startService()

endpoint_description = f"tcp6:port={port}:interface={host}"
endpoint = endpoints.serverFromString(reactor, endpoint_description)
site = Site(app.resource())
site.displayTracebacks = True

endpoint.listen(site)
reactor.run()
